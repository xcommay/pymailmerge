#pyMailMerge (pyMMM)
## Sample markdown file

This is a sample markdown file to be used as an exaple input markdown file for pyMMM.py.

The variables contained in this file are v1, v2, v3, v4. 

The value for v1 is $v1$. 

The value for v2 is $v2~$.

The value for v3 is $v3$.

The value for v4 is $v4$.

As v2 is a list (specified by the dictioonary / input data file) it will be added sequentially. So the value of v2 here will be $v2~$.

