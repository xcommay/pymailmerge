"""
Wishva Herath
XCommaY

python mail merge (to) markdown.

"""
     
    
class addVarsToMarkdown():
    """
    Adds variables from a given dictionary into a markdown file so that custom reports can be written.
    
    Parameters
    ----------
    prefix: prefix of the variable marker
    postfix: postfix of the variable marker (prefix + variable_name + postfix)
    seqfix: marker for sequantial variables. When a variable is marked with this it will add values of a list sequentially.
    
    
    """
    def __init__(self,prefix="$", postfix="$", seqfix = "~"):
        self.prefix = prefix
        self.postfix = postfix
        self.seqfix = "~"
        
    def injectVariablesFromDict(self,markdown_file_name,var_dict, save_file = True,new_file_name = None):
        """
        Adds variables to markers (format = prefix + varname + postfix) from a given list.
        
        if sequences is True - will add values of a list sequentially to the markers.
        
        Parameters
        ----------
        
        markdown_file_name: The markdown file which contains the formatting and variable markers
        var_dict: dict,  dictionary of variables
        save_file: bool default True. Saves the markdown file with the variable values added with the file name = markdown_file_name + "_with_values.md"
                if not returns the modified text.
        new_file_name: the new file name, if this is not specified but save_file is True then the file name of the markup file will be used. 
                
        Returns
        -------
        
        saved file: if save_file == True if not modified text
        """
        
        self.validateMarkdown(markdown_file_name, var_dict)
        
        print '...',markdown_file_name
        md = open(markdown_file_name, "r") 
        text = md.read() #dump the entire file contents into text.
        
        
        for var in var_dict:
            if type(var_dict[var]) == list:
                #adding the list as a sequence.
                var_list = var_dict[var]
                search = self.prefix + str(var) + self.seqfix + self.postfix

                for v in var_list:
                    replace = str(v)
                    text = text.replace(search,replace,1)
            
            else:
                #normal replace
                search = self.prefix + str(var) + self.postfix
                replace = str(var_dict[var])
                text = text.replace(search,replace)
                
        if save_file == True and new_file_name == None:
            fn = markdown_file_name + "_with_values.md"
            f = open(fn,"w")
            f.write(text)
            f.close()
        
        elif new_file_name != None:
            fn = new_file_name
            f = open(fn,"w")
            f.write(text)
            f.close()
        
        else:
            return text
        
        md.close()
        
        
    def injectVariablesFromFile(self,markdown_file_name,data_file,sep = "\t"):
        
        """
        Reads a file and adds the values (in columns ) into seperate markdown files.
        
        Parameters
        ----------
        
        markdown_file_name: original file which describes the format
        data_file: the file containing the values to be entered into the above file. The first column of the file should contain a unique identifier.
        sep: delimiter of data_file, default tab.
        
        Returns
        -------
        
        Generates a dictionary for each row and calls injectVariablesFromDict. The new file is named according to the id coulumn. 
        
        """
        
        i = 0
        for line in open(data_file,"r"):
            if i == 0:
                #first line
                header_list = line.strip().split(sep)
                header_list = header_list[1:] #removes the id name
                i = i + 1
            else:
                val_list = line.strip().split(sep)
                id = val_list[0] #removes 
                val_list = val_list[1:] #removes id 
                #identify lists for sequence insertion
                
                val_list2 = []
                for val in val_list:
                    if val.strip()[0] == '[':
                        #list
                        v = val[1:-1].strip().split(',')
                        val_list2.append(v)
                    else:
                        val_list2.append(val)
                
                val_list = val_list2
                
                
                var_dict = {}
                
                
                for var,val in zip(header_list,val_list):
                    var_dict[var] = val
                    
                    
                new_file_name = str(id) + '.md'
                print '...'
                print var_dict
                print new_file_name
                
                self.injectVariablesFromDict(markdown_file_name,var_dict,True, new_file_name)
                #def injectVariablesFromDict(self,markdown_file_name,var_dict, save_file = True,new_file_name = None):
                
        
        
        
    def validateMarkdown(self,markdown_file_name, var_dict):
        import re
        regex = re.compile("\$.*?\$")
        
        
        md = open(markdown_file_name, "r") 
        text = md.read() #dump the entire file contents into text.
        
        var_list = regex.findall(text)
                
        var_list = [v[1:-1] for v in var_list] #removing the $ signs
        var_list = [v[:-1] if "~" in v else v for v in var_list]
        
        vl = var_dict.keys()
        if not set(var_list) == set(vl):
            print "warning! there are some variables in markdown file that are not accounted for in the dict."
        
    
    
    
if __name__ == '__main__':
    import sys
    try:
        markdown_file_name = sys.argv[1].strip()
        data_file = sys.argv[2].strip()
    except:
        print "format - python markdown_file_name, data_file"
        sys.exit(1)
    
    m = addVarsToMarkdown()
    m.injectVariablesFromFile(markdown_file_name ,data_file)
    #m.injectVariablesFromDict("testFile.md",var_dict)
    
    
    